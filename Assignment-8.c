/* Write down a program to store the details of students using the knowledge of loops, arrays and structures you gained so far.
   First name of the student, subject and its marks for each student needs to be recorded.
   Data should be fetched through user input (do not hard code) and print the data back. Program should record and display
   information of minimum 5 students. */

#include<stdio.h>

struct student
{
    char name[50];
    char subject[50];
    int num;
    float marks;
}s[5];

int main()
{
    int i;

    printf("Enter Information: \n");

    for(i=0; i<5; ++i)
    {
        s[i].num = i+1;

        printf("\nEnter Name: ");
        scanf("%s",s[i].name);

        printf("Enter Subject: ");
        scanf("%s",s[i].subject);

        printf("Enter Marks: ");
        scanf("%f",&s[i].marks);

    }

    printf("\nDisplay Information: \n");

    for(i=0; i<5; ++i)
    {
        printf("\nName: ");
        puts(s[i].name);
        printf("Subject: ");
        puts(s[i].subject);
        printf("Marks: %.1f",s[i].marks);
        printf("\n");

    }

    return 0;
}
